import com.squareup.javapoet.*
import com.squareup.javapoet.ParameterSpec
import com.squareup.javapoet.TypeName
import uk.co.danielbaxter.stringgenerator.StringCodeGeneratorConfig


import java.io.File
import javax.lang.model.element.Modifier

/**
 * Generates the kotlin class to get the resources from
 */
class StringsCodeGenerator {

    private val snakeCaseRegex = """_[a-zA-Z]""".toRegex()

    // The methods being generated will be delegated to Android's context.getString(...) method, which requires a vararg param
    // These are by default nullable, but the kotlin compiler flat out ignores nullable varargs, and with the different signature
    // breaks the functionality. So have to generate java code instead
    fun buildStringsRepository(config: StringCodeGeneratorConfig, packageName: String, outputLocation: String) {
        val file = JavaFile.builder(packageName,
            TypeSpec.classBuilder(CLASS_NAME)
                    .addJavadoc("Wrapper class around string resources, allowing their usage in the presentation layer and unit tests")
                    .addModifiers(Modifier.PUBLIC)
                    .addField(FieldSpec.builder(ClassName.get("android.content", "Context"), "context", Modifier.PRIVATE).build())
                    .addMethod(
                            MethodSpec.constructorBuilder()
                                    .addModifiers(Modifier.PUBLIC)
                                    .addParameter(ClassName.get("android.content", "Context"), "context")
                                    .addStatement("this.context = context")
                                    .build()
                    )
                    .addStringGetters(config.stringResourceNames)
                    .addPluralsGetters(config.pluralsResourceNames)
                    .build()
                ).build()

        file.writeTo(File(outputLocation))
    }

    private fun resourceNameToFunctionName(resourceName: String): String
        = "get${snakeCaseRegex.replace(resourceName.trim('_')) { it.value.replace("_", "").toUpperCase() }.capitalize()}"

    private fun TypeSpec.Builder.addStringGetters(stringResourceNames: List<String>): TypeSpec.Builder {
        stringResourceNames
                .map { name -> name to resourceNameToFunctionName(name) }
                .distinctBy { (_, funName) -> funName }
                .forEach { (resName, funName) ->
                    addMethod(
                            MethodSpec.methodBuilder(funName)
                                    .addModifiers(Modifier.PUBLIC)
                                    .returns(String::class.java)
                                    .addParameter(ParameterSpec.builder(ArrayTypeName.get(Array<Any>::class.java), "args").build())
                                    .varargs(true)
                                    .addStatement("return context.getString(R.string.$resName, args)")
                                    .build()
                    )
                }

        return this
    }

    private fun TypeSpec.Builder.addPluralsGetters(pluralsResourceNames: List<String>): TypeSpec.Builder {
        pluralsResourceNames
                .map { name -> name to resourceNameToFunctionName(name) }
                .distinctBy { (_, funName) -> funName }
                .forEach { (resName, funName) ->
                    addMethod(
                            MethodSpec.methodBuilder(funName)
                                    .addModifiers(Modifier.PUBLIC)
                                    .returns(String::class.java)
                                    .addParameter(ParameterSpec.builder(TypeName.INT, "quantity").build())
                                    .addParameter(ParameterSpec.builder(ArrayTypeName.get(Array<Any>::class.java), "args").build())
                                    .varargs(true)
                                    .addStatement("return context.getResources().getQuantityString(R.plurals.$resName, quantity, args)")
                                    .build()
                    )
                }

        return this
    }

    companion object {
        private const val CLASS_NAME = "AppStrings"
    }
}
