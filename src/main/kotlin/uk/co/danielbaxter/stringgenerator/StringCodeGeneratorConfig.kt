package uk.co.danielbaxter.stringgenerator

/**
 * The configuration that the code generator needs
 */
data class StringCodeGeneratorConfig(
    val stringResourceNames: List<String>,
    val pluralsResourceNames: List<String>)
