package uk.co.danielbaxter.stringgenerator

import jdk.internal.org.objectweb.asm.Opcodes
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.FieldVisitor

class RClassVisitor(private val onFinished: (List<String>) -> Unit): ClassVisitor(Opcodes.ASM4) {

    private val classFieldNames = mutableListOf<String>()

    override fun visitField(
        access: Int,
        name: String,
        descriptor: String?,
        signature: String?,
        value: Any?
    ): FieldVisitor {
        classFieldNames.add(name)
        return object: FieldVisitor(Opcodes.ASM4) {}
    }

    override fun visitEnd() {
        onFinished(classFieldNames)
        super.visitEnd()
    }
}