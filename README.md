# Android String Generator

A gradle plugin to auto-generate a wrapper class around Android string resources at build time.



### Setup

Add the plugin to your app/module level `build.gradle`:

```groovy
plugins {
    id 'uk.co.danielbaxter.stringgenerator' version '1.0.2'
}
```

### Usage

This plugin will add a new gradle task for each of your build variants, which will run automatically whenever your
app builds, or can be run separately. Default projects will include `generateDebugStrings` and `generateReleaseStrings`.

At build time, this will generate an `AppStrings.java` file that includes a getter method for every defined string/plurals resource

```java
public class AppStrings {
 
    private Context context;
    
    public AppStrings(Context context) {
        this.context = context;
    }
  
    public String getStringOne(Object... args) {
        return context.getString(R.string.string_one, args);
    }

    public String getStringTwo(Object... args) {
        return context.getString(R.string.string_two, args);
    }

    public String getPluralsOne(int quantity, Object... args) {
        return context.resources.getQuantityString(R.plurals.plurals_one, quantity, args);
    }
    
    //...
}
```

It enables string resource usage in the presentation layer, as the returned values can easily be 
mocked in unit tests

### Notes

#### BuildScript Resources

Any string resources added manually in your gradle buildscript will be generated too, but will cause crashes if accessed at runtime 
(as the R class excludes them). In the `AppStrings.java` file, they will show as an error, but won't stop your app building.